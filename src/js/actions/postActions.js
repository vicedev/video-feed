import { CALL_API } from '../middleware/api'

/**
 * Post lists
 */

function fetchPosts(types, endpoint, attribute = null) {
  return {
    attribute,
    [CALL_API]: {
      types: types,
      endpoint: endpoint
    }
  }
}

/**
 * Latest posts
 */

export const LATEST_POSTS_REQUEST = 'LATEST_POSTS_REQUEST'
export const LATEST_POSTS_RECIEVE = 'LATEST_POSTS_RECIEVE'
export const LATEST_POSTS_FAILURE = 'LATEST_POSTS_FAILURE'

export function loadLatestPosts(nextPage = false) {
  return (dispatch, getState) => {
    let {
      nextPageUrl = `/videos?locale=en_us&status_id=3`,
      pageCount = 0
    } = getState().videos.latest || {}

    if(pageCount > 0 && !nextPage) {
      return null;
    }

    if(pageCount > 0) {
      nextPageUrl = `/videos?locale=en_us&status_id=3&not_channel_id=57a204098cb727dec794c6a3&page=${pageCount+1}`
    }

    const types = [
      LATEST_POSTS_REQUEST,
      LATEST_POSTS_RECIEVE,
      LATEST_POSTS_FAILURE
    ]

    return dispatch(fetchPosts(
      types, 
      nextPageUrl
    ))
  }
}

/**
 * Trending posts
 */

export const TRENDING_POSTS_REQUEST = 'TRENDING_POSTS_REQUEST'
export const TRENDING_POSTS_RECIEVE = 'TRENDING_POSTS_RECIEVE'
export const TRENDING_POSTS_FAILURE = 'TRENDING_POSTS_FAILURE'

export function loadTrendingPosts(nextPage = false) {
  return (dispatch, getState) => {
    let {
      nextPageUrl = `/trending?locale=en_us&status_id=3`,
      pageCount = 0
    } = getState().videos.trending || {}

    if(pageCount > 0 && !nextPage) {
      return null;
    }

    if(pageCount >= 1 ) {
      nextPageUrl = `/trending?locale=en_us&status_id=3&sort=episode_display_mode&not_channel_id=57a204098cb727dec794c6a3&page=${pageCount+1}`
    }

    const types = [
      TRENDING_POSTS_REQUEST,
      TRENDING_POSTS_RECIEVE,
      TRENDING_POSTS_FAILURE
    ]

    return dispatch(fetchPosts(
      types, 
      nextPageUrl
    ))
  }
}