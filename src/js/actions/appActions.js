export const TOGGLE_LAYOUT = 'TOGGLE_LAYOUT';
export const TOGGLE_TOOLBAR = 'TOGGLE_TOOLBAR';

export function toggleLayout() {
  return (dispatch, getState) => {
    let layout;
    const { app } = getState();
    
    if(app.layout == 'grid') {
      layout = 'feed';
    } else {
      layout = 'grid';
    }

    return dispatch({
      type: TOGGLE_LAYOUT,
      layout: layout
    })
  }
}

export function toggleToolbar() {
  return (dispatch, getState) => {
    let toolbar;
    const { app } = getState();
    
    if(!app.toobar) {
      toolbar = true;
    } else {
      toolbar = false;
    }

    return dispatch({
      type: TOGGLE_TOOLBAR,
      toolbar: tollbar
    })
  }
}