export default function renderHtml(config) {
  return `
    <!doctype html>
    <html>
      <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
      </head>
      <body>
        <div id="app"></div>
        <script src="${config.siteUrl}public/bundle.js"></script>
      </body>
    </html>
  `
}
