import React, { Component, PropTypes } from 'react';
import { toggleLayout } from '../actions/appActions';
import FeedItem from './FeedItem';
import classnames from 'classnames';
import Toolbar from '../containers/Toolbar';
 
class FeedList extends Component {
  constructor(props) {
    super(props);

    this.loadMorePosts = this.loadMorePosts.bind(this);
  }

  componentDidMount() {
    const {
      loadInitialPosts
    } = this.props;

    loadInitialPosts();
  }

  loadMorePosts() {
    const {
      loadMorePosts
    } = this.props;

    loadMorePosts();
  }

  render() {
    const {
      isFetching,
      items,
      pageCount
    } = this.props.videos;

    const {
      toolbar
    } = this.props.app;

    let videoItems = items.map((video, i) => {
      let normalizedVideo;

      // some items are within the data object
      // so just need to normalise that
      if(typeof video.data === 'undefined') {
        normalizedVideo = video;
      } else {
        normalizedVideo = video.data;
      }

      return <FeedItem key={ i } video={ normalizedVideo } />
    })

    const videoFeedClasses = classnames('videoFeed', {
      'videoFeed--withToolbar': toolbar
    })

    const loaderClasses = classnames('loader', {
      'loader--centered': pageCount == 0
    })

    return (
      <div>
        {toolbar && 
          <Toolbar />
        }
        <div className={ videoFeedClasses }>
          <div className="videoFeed__items">
            { videoItems }
          </div>
          <div className="loadMoreSection">
            <div className="loadMoreSection__inner">
              {!isFetching &&
                <button className="btn btn--fullWidth btn--loadMore" onClick={ this.loadMorePosts }>Load More</button>
              }
              {isFetching &&
                <div className={ loaderClasses }></div>
              }
            </div>
          </div>
        </div>
      </div>
    )
  } 
}
 
export default FeedList