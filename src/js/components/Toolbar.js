import React, { Component, PropTypes, createRef } from 'react';
import classnames from 'classnames';
import { loadTrendingPosts } from '../actions/postActions';
import { Link } from 'react-router-dom';

const menuItems = [
  {
    label: 'Politics',
    id: '57a204ab8cb727dec7951a9d'
  },
  {
    label: 'Entertainment',
    id: '57a204c98cb727dec79531a4'
  },
  {
    label: 'Trending',
    id: '57a205178cb727dec7956d45'
  },
  {
    label: 'Funny',
    id: '57a204a58cb727dec7951606'
  },
  {
    label: 'News',
    id: '57a204af8cb727dec7951d13'
  },
  {
    label: 'NSFW',
    id: '57a204ea8cb727dec7954ac2'
  },
  {
    label: 'Music',
    id: '57a204a98cb727dec79518b3'
  },
  {
    label: 'Technology',
    id: '57a204ad8cb727dec7951bdd'
  },
  {
    label: 'Livestreams',
    id: '57a206847bc756e020b6180a'
  }
]

const MenuItem = ({ item, handleMenuItem }) => (
  <li onClick={ handleMenuItem }>
    <Link to={`/category/${item.id}`}>
      { item.label }
    </Link>
  </li>
)

class Toolbar extends Component {
  constructor(props) {
    super(props);

    this.handleMenuItem = this.handleMenuItem.bind(this);
  }

  handleMenuItem(id) {
    const {
      dispatch
    } = this.props;

    dispatch(loadTrendingPosts());
  }

  render() {
    return (
      <div className="toolbar">
        <ul className="toolbar__menu">
          <li>
            <Link to="/latest">Latest</Link>
          </li>
          <li>
            <Link to="/trending">Trending</Link>
          </li>
          { menuItems.map((item, i) => <MenuItem key={ i } item={ item } handleMenuItem={ this.handleMenuItem.bind(item.id) } />) }
          <li>VICE Video →</li>
        </ul>
      </div>
    )
  }
}

export default Toolbar;


