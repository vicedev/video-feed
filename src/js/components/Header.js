import React, { Component, PropTypes } from 'react';

const Header = () => (
  <div className="headerContainer">
    <div className="header">
      <img className="header__logo" src="https://vice-web-statics-cdn.vice.com/logos/vice_logo_black.svg" alt="VICE" />
    </div>
  </div>
)

export default Header;