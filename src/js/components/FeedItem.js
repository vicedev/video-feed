import React, { Component, PropTypes, createRef } from 'react';
import classnames from 'classnames';
import VisibilitySensor from 'react-visibility-sensor';
import moment from 'moment';

function toHHMMSS(time) {
  var sec_num = parseInt(time, 10); // don't forget the second param
  var hours   = Math.floor(sec_num / 3600);
  var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
  var seconds = sec_num - (hours * 3600) - (minutes * 60);

  if (hours   < 10) {hours   = "0"+hours;}
  if (minutes < 10) {minutes = "0"+minutes;}
  if (seconds < 10) {seconds = "0"+seconds;}
  return hours+':'+minutes+':'+seconds;
}

class FeedItem extends Component {
  constructor(props) {
    super(props);

    this.state = {
      iframeHasLoaded: false,
      isVisible: false
    }

    this.loadListener = this.loadListener.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  loadListener() {
    this.setState({
      iframeHasLoaded: true
    })
  }

  onChange(isVisible) {
    // in here you can play pause the video when it's in / out of view

    this.setState({
      isVisible: isVisible
    })
  };

  render() {
    const {
      video
    } = this.props;

    const iframeClasses = classnames({
      hasLoaded: this.state.iframeHasLoaded
    })

    return (
      <div className="feedUnit">
        <div className="videoUnit" key={video.id}>
          <VisibilitySensor onChange={this.onChange}>
            <div className="videoUnit__container">
              <iframe className={iframeClasses} onLoad={this.loadListener} src={`${video.embed_url}?showinfo=0&continuous=0`} border="0" allowFullScreen />
              {!this.state.iframeHasLoaded &&
                <div className="videoUnit__loader">
                  <div className="loader loader--light"></div>
                </div>
              }
            </div>
          </VisibilitySensor>
        </div>
        <div className="videoInfo">
          <h2 dangerouslySetInnerHTML={{ __html: video.title }} ></h2>
          <p className="videoInfo__summary">{video.dek}</p>
          
          <span className="videoInfo__duration">From <a href={ video.episode.season.show.url } target="_blank">{ video.episode.season.show.title }</a></span>
          <span className="videoInfo__duration">Published { moment(video.publish_date).format("MMM D YYYY") } </span>
          <span className="videoInfo__duration">{ toHHMMSS(video.duration) } — Link to this video →</span>
          
          <a href={video.episode.season.show.urls[0]} target="_blank" className="btn moreLikeThisBtn">
            More like this  →
          </a>
        </div>
      </div>
    )
  }
}

export default FeedItem;