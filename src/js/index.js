import { render } from 'react-dom';
import React from 'react';
import styles from '../css/index.scss';

import LatestFeed from './containers/LatestFeed';
import TrendingFeed from './containers/TrendingFeed';
import Header from './components/Header';

// Redux
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import reducers from './reducers';

import createHistory from 'history/createBrowserHistory'
import { Route, IndexRoute, Switch } from 'react-router'
import { TransitionGroup, CSSTransition } from 'react-transition-group';

import { ConnectedRouter, routerMiddleware, push } from 'react-router-redux';

import api from './middleware/api';
import thunk from 'redux-thunk'

import ScrollToTop from './components/ScrollToTop';

const history = createHistory();
const middleware = routerMiddleware(history);

const store = createStore(
  reducers,
  applyMiddleware(
    thunk,
    api,
    middleware
  ))

render(
  <Provider store={store}>
    <ConnectedRouter onUpdate={() => console.log('change')} history={history}>
      <ScrollToTop>
        <div className="page">
          <Header />
          <TransitionGroup>
             <CSSTransition key={location.key} classNames="fade" timeout={1000}> 
              <Switch>
                <Route exact path="/" component={LatestFeed} />
                <Route path="/latest" component={LatestFeed} />
                <Route path="/trending" component={TrendingFeed} />
                <Route path="/category/:id" component={TrendingFeed} />
              </Switch>
            </CSSTransition>
          </TransitionGroup>
        </div>
      </ScrollToTop>
    </ConnectedRouter>
  </Provider>,
  document.getElementById('app')
)