import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { loadTrendingPosts } from '../actions/postActions';
import FeedList from '../components/FeedList';

function mapStateToProps(state) {
  const videos = state.videos['trending'];
  const app = state.app;

  return {
    videos,
    app
  }
}

function mapDispatchToProps(dispatch, state) {
  return {
    loadInitialPosts: () => { dispatch(loadTrendingPosts()) },
    loadMorePosts: () => { dispatch(loadTrendingPosts(true)) }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FeedList)