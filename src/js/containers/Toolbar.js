import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import Toolbar from '../components/Toolbar';

const mapStateToProps = state => (state);

export default connect(
  mapStateToProps
)(Toolbar)