import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { loadLatestPosts } from '../actions/postActions';
import FeedList from '../components/FeedList';

function mapStateToProps(state) {
  const videos = state.videos['latest'];
  const app = state.app;

  return {
    videos,
    app
  }
}

function mapDispatchToProps(dispatch, state) {
  return {
    loadInitialPosts: () => { dispatch(loadLatestPosts()) },
    loadMorePosts: () => { dispatch(loadLatestPosts(true)) }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FeedList)