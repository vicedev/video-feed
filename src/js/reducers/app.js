import merge from 'lodash/merge';
import {
  TOGGLE_LAYOUT,
  TOGGLE_TOOLBAR
} from '../actions/appActions';

const app = function(state = {
    layout: 'feed',
    toolbar: true,
    incognito: true
  }, action) {
  switch (action.type) {
    case TOGGLE_LAYOUT:
      return merge({}, state, {
        layout: action.layout
      })
    case TOGGLE_TOOLBAR:
      return merge({}, state, {
        toolbar: action.layout
      })
    default:
      return state
  }
}

export default app;