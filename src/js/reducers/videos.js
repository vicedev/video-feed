import union from 'lodash/union'
import { combineReducers } from 'redux'
import {
  LATEST_POSTS_REQUEST, LATEST_POSTS_RECIEVE, LATEST_POSTS_FAILURE,
  TRENDING_POSTS_REQUEST, TRENDING_POSTS_RECIEVE, TRENDING_POSTS_FAILURE
} from '../actions/postActions'
import paginate from './paginate'

const videos = combineReducers({
  latest: paginate({ 
    types: [ 
      LATEST_POSTS_REQUEST,  
      LATEST_POSTS_RECIEVE,  
      LATEST_POSTS_FAILURE
    ]
  }),
  trending: paginate({ 
    types: [ 
      TRENDING_POSTS_REQUEST,  
      TRENDING_POSTS_RECIEVE,  
      TRENDING_POSTS_FAILURE
    ]
  })
})

export default videos