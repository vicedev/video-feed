import { combineReducers } from 'redux';
import videos from './videos';
import app from './app';

import { routerReducer } from 'react-router-redux';

const videoFeedApp = combineReducers({
  videos,
  app,
  router: routerReducer
})

export default videoFeedApp;