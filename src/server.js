import wpConfig from './webpack.config.js';
import webpack from 'webpack';
import webpackMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import express from 'express';
const app = express();
import renderHtml from './js/helpers/renderHtml';

const isProduction = process.env.NODE_ENV === 'production';
const port = !isProduction ? 5000 : process.env.PORT;

const siteConfig = {
  siteUrl: `//localhost:${port}/`
}

app.use('/', express.static(__dirname + '/public/'))

if(!isProduction) {
  // Webpack
  const compiler = webpack(wpConfig)
  const middleware = webpackMiddleware(compiler, {
    publicPath: wpConfig.output.publicPath,
    contentBase: 'src',
    stats: {
      colors: true,
      hash: false,
      timings: true,
      chunks: false,
      chunkModules: false,
      modules: false
    }
  })
  app.use(middleware)
  app.use(webpackHotMiddleware(compiler))
}

app.use('/', function(req, res) {
	res.send(renderHtml(siteConfig))
})

app.listen(`${port}`, function(err) {
	if (err) {
	  console.log(err)
	}
	console.info(`🌎 => Listening on port ${port}. Open up http://127.0.0.1:${port}/ in your browser. Prod = ${isProduction} `);
});