import webpack from 'webpack';
import path from 'path';

export default {
  externals: {
    'window': 'window'
  },
  context: path.join(__dirname, 'js'),
  entry: [
    'webpack-hot-middleware/client?reload=true',
    './index.js'
  ],
  output: {
    path: path.join(__dirname, '/public/'),
    filename: 'bundle.js',
    publicPath: '/public/'
  },
  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin()
  ],
  module: {
    loaders: [{
      test: /\.js?$/,
      exclude: /node_modules/,
      loader: 'babel-loader'
    }, { 
      test: /\.json$/, 
      loader: 'json-loader' 
    }, {
      test: /\.scss$/,
      use: [{
          loader: "style-loader" 
      }, {
          loader: "css-loader"
      }, {
          loader: "sass-loader"
      }]
    }]
  }
}